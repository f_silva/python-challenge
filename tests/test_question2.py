import time
import random
from src.question2 import Orders


def test_default_case():
    orders = [70, 30, 10]
    n_max = 100
    expected_orders = 2

    how_many = Orders().combine_orders(orders, n_max)

    assert how_many == expected_orders


def test_with_many_orders(range_value, n_max):
    orders = []
    for i in range(1, range_value):
        orders.append(random.randint(10, 100))

    start_time = time.time()
    Orders().combine_orders(orders, n_max)
    end_time = time.time()

    execution_time = end_time - start_time
    print(f"Execution time with {range_value} orders and maximum_value {n_max}:", execution_time)


test_default_case()

test_with_many_orders(10, 100) # test with 10 orders and maximum value 100
test_with_many_orders(1000, 100) # test with 1000 orders and maximum value 100
test_with_many_orders(1000000, 100) # test with 1000000 orders and maximum value 100
