import time
import random
from src.question1 import Contract, Contracts


def test_with_many_contracts(range_value, renegotiated_contracts_quantity, top_n):
    contracts = []
    renegotiated_contract_ids = []

    for i in range(0, range_value):
        contracts.append(Contract(i, random.randint(1, 1000)))

    for i in range(0, renegotiated_contracts_quantity):
        renegotiated_contract_ids.append(Contract(i, random.randint(1, range_value)))

    start_time = time.time()
    Contracts.get_top_N_open_contracts(Contracts, contracts, renegotiated_contract_ids, top_n)
    end_time = time.time()

    execution_time = end_time - start_time
    print(f"Execution time with {range_value} contracts, {renegotiated_contracts_quantity} renegotiations, top {top_n} values :", execution_time)


def test_default_case():
    contracts = [
        Contract(1, 1),
        Contract(2, 2),
        Contract(3, 3),
        Contract(4, 4),
        Contract(5, 5)
    ]

    renegotiated = [3]
    top_n = 3

    actual_open_contracts = Contracts.get_top_N_open_contracts(
        Contracts, contracts, renegotiated, top_n
    )

    expected_open_contracts = [5, 4, 2]

    assert expected_open_contracts == actual_open_contracts


test_default_case()

test_with_many_contracts(10, 3, 3) # test with 10 contracts, 3 renegotiated contracts, top 3 highest debts
test_with_many_contracts(1000, 300, 30) # test with 1000 contracts, 300 renegotiated contracts, top 30 highest debts
test_with_many_contracts(1000000, 30000, 300) # test with 1000000 contracts, 30000 renegotiated contracts, top 300 highest debts
