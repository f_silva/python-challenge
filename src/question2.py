class Orders:
    def combine_orders(self, requests, n_max):
        ordered_requests = sorted(requests)

        min_value_index = 0
        max_value_index = len(ordered_requests) - 1
        sorted_trips = []

        while min_value_index <= max_value_index:
            current_max_value = ordered_requests[max_value_index]
            current_min_value = ordered_requests[min_value_index]
            summed_values = current_min_value + current_max_value

            if max_value_index == min_value_index:
                sorted_trips.append(ordered_requests[min_value_index])
                break
            if summed_values > n_max:
                sorted_trips.append(max_value_index)
                max_value_index -= 1
            if summed_values <= n_max:
                sorted_trips.append((current_max_value, current_min_value))
                max_value_index -= 1
                min_value_index += 1

        return len(sorted_trips)
