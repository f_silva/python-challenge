class Contract:
    def __init__(self, id, debt):
        self.id = id
        self.debt = debt

    def __str__(self):
        return 'id={}, debt={}'.format(self.id, self.debt)


class Contracts:
    def get_top_N_open_contracts(self, open_contracts, renegotiated_contracts, top_n):
        non_renegotiated_contracts = [contract for contract in open_contracts if any(contract.id != renegotiated_id for renegotiated_id in renegotiated_contracts)]

        sorted_contracts_ids = [c.id for c in sorted(non_renegotiated_contracts, key=lambda contract: contract.debt, reverse=True)]

        return sorted_contracts_ids[:(top_n)]
