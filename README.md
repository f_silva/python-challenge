## Python technical challenge

O repositório apresenta a solução para as duas questões propostas, cujas instruções estão na pasta `/src`. As respostas estão nos arquivos `question1.py` e `question2.py`.

A performance de cada questão pode ser verificada conforme a imagem, para diferentes cenários:

![performance test results](https://gitlab.com/f_silva/python-challenge/-/raw/master/imgs/performance_screenshot.png)

Para realizar novos testes em diferentes cenários, basta alterar os valores dos parâmetros nas chamadas das funções no final dos arquivos de teste:
* `tests/test_question1.py`
* `tests/test_question2.py`

Os arquivos incluem ainda testes para verificar que o código funciona no cenário default, dado como exemplo nas instruções.

Para rodar os testes, executar na linha de comando, respectivamente:
* `python3 -m tests.test_question1`
* `python3 -m tests.test_question2`
